﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

[Serializable]
public class RiddleObject
{
    public string riddle;
    public string answer;
}

public class RiddleProvider : MonoBehaviour
{
    public GameObject RiddlePanel, DeliveredPanel, AnsweredPanel;
    public TextMeshProUGUI RiddleHolder;
    public TextMeshProUGUI CorrectAnwser;
    public TextMeshProUGUI Title;
    public Button Accept, Refuse;
    public TMP_InputField AnwserField;
    public Button SendButton;
    public Button ShuffleButton;
    public string CurrentRiddle;
    public UnityEvent<string> ReceiveRiddle = new();
    public UnityEvent<string> SendAnswer = new();

    public RiddleObject CurrentRiddleObj;
    public static RiddleProvider Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void ClearText()
    {
        RiddleHolder.text = "";
        CorrectAnwser.text = "";
        AnwserField.text = "";
    }
    
    public IEnumerator GetRiddle()
    {
        ClearText();
        do
        {
            var www = UnityWebRequest.Get("https://riddles-api.vercel.app/random");
            yield return www.SendWebRequest();
            CurrentRiddleObj = JsonUtility.FromJson<RiddleObject>(www.downloadHandler.text);
        } while (CurrentRiddleObj.answer.Length > 50);
            
        CurrentRiddle = CurrentRiddleObj.riddle;
        CorrectAnwser.text = CurrentRiddleObj.answer;
        RiddleHolder.text = CurrentRiddle;
        DeliveredPanel.SetActive(false);
        SendButton.interactable = true;
    }

    public void ReceiveState()
    {
        Title.text = "RECEIVE RIDDLES";
        ShuffleButton.gameObject.SetActive(false);
        AnwserField.interactable = true;
        SendButton.onClick.RemoveAllListeners();
        SendButton.onClick.AddListener(HandleSendAnswer);
    }

    private void HandleSendAnswer()
    {
        SendAnswer.Invoke(AnwserField.text);
        AnsweredPanel.SetActive(true);
    }

    public void SendState()
    {
        Title.text = "DELIVER RIDDLES";
        ShuffleButton.gameObject.SetActive(true);
        StartCoroutine(GetRiddle());
        ShuffleButton.onClick.AddListener(() => StartCoroutine(GetRiddle()));
        AnwserField.interactable = false;
        SendButton.onClick.RemoveAllListeners();
        SendButton.onClick.AddListener(SendRiddle);
    }

    public void SendRiddle()
    {
        ReceiveRiddle.Invoke(CurrentRiddle);
        DeliveredPanel.SetActive(true);
    }
    
    public void ToggleRiddlePanel(bool isActive)
    {
        RiddlePanel.SetActive(isActive);
    }

    public static bool Correct(string input, string target)
    {
        int result = Math.Abs(input.Length - target.Length);
        return target.Contains(input) && result < 5;
    }
}