using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;

public class LobbyProvider : MonoBehaviour
{
    public TMP_InputField NameInputField;
    public Button HandleCreateButton;
    public Button HandleRefreshButton;
    public Button QuitLobby;
    public LobbyObject LobbyObjectTemplate;

    public Dictionary<string, LobbyObject> ActiveLobbies = new();

    public static LobbyProvider Instance;

    private Lobby _currentLobby;

    public void Awake()
    {
        Instance = this;
    }

    public void Start()
    {
        if (!NameInputField || !HandleCreateButton || !HandleRefreshButton) return;

        HandleCreateButton.onClick.AddListener(CreateLobby);
        HandleRefreshButton.onClick.AddListener(GetLobbies);
    }

    private async void CreateLobby()
    {
        var lobby = Instantiate(LobbyObjectTemplate, LobbyObjectTemplate.transform.parent);
        lobby.gameObject.SetActive(true);
        var nameLobby = NameInputField.text == "" ? "Player" : NameInputField.text;
        await lobby.Host(nameLobby);
        ActiveLobbies.Add(lobby.GetLobbyId(), lobby);
        var id = lobby.GetLobbyId();
        QuitLobby.onClick.AddListener(() => HandleQuitHostLobby(id));
    }


    private async void HandleQuitHostLobby(string id)
    {
        RiddleProvider.Instance.ClearText();
        QuitLobby.onClick.RemoveAllListeners();
        Destroy(ActiveLobbies[id].gameObject);
        await LobbyService.Instance.DeleteLobbyAsync(id);
        GetLobbies();
        RiddleProvider.Instance.ToggleRiddlePanel(false);
    }

    public async void GetLobbies()
    {
        var lobbies = await QueryAllLobby();
        var listIds = new List<string>();
        foreach (var lobby in lobbies)
        {
            if (!ActiveLobbies.ContainsKey(lobby.Id))
                JoinLobby(lobby);
            else
            {
                ActiveLobbies[lobby.Id].HandleJoin(lobby);
            }

            listIds.Add(lobby.Id);
        }

        var toRemove = new List<string>();

        foreach (var activeLobby in ActiveLobbies)
        {
            if (!listIds.Contains(activeLobby.Value.GetLobbyId()))
            {
                toRemove.Add(activeLobby.Key);
            }
        }

        foreach (var id in toRemove)
        {
            var obj = ActiveLobbies[id];
            Destroy(obj.gameObject);
            ActiveLobbies.Remove(id);
        }
    }

    private void JoinLobby(Lobby lobby)
    {
        var l = Instantiate(LobbyObjectTemplate, LobbyObjectTemplate.transform.parent);
        l.gameObject.SetActive(true);
        l.HandleJoin(lobby);
        l.SelectButton.onClick.AddListener(l.JoinLobby);
        QuitLobby.onClick.AddListener(() => HandleQuitClientLobby(lobby.Id));
        ActiveLobbies.Add(lobby.Id, l);
    }

    private async void HandleQuitClientLobby(string lobbyId)
    {
        RiddleProvider.Instance.ClearText();
        QuitLobby.onClick.RemoveAllListeners();
        await LobbyService.Instance.RemovePlayerAsync(lobbyId, AuthenticationService.Instance.PlayerId);
        RiddleProvider.Instance.ToggleRiddlePanel(false);
        GetLobbies();
    }

    private async Task<List<Lobby>> QueryAllLobby()
    {
        var options = new QueryLobbiesOptions
        {
            Count = 25,
            Skip = 0,
            SampleResults = false,
            Filters = null,
            Order = null,
            ContinuationToken = null
        };
        var result = await Lobbies.Instance.QueryLobbiesAsync(options);
        return result.Results;
    }
}