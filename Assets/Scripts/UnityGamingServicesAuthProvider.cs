﻿
using Unity.Services.Authentication;
using Unity.Services.Core;
using UnityEngine;
using UnityEngine.Events;

public class UnityGamingServicesAuthProvider : MonoBehaviour
{
    public static UnityEvent HandleSignIn = new();
    
    private void Awake()
    {
        SignIn();
    }

    private async void SignIn()
    {
        await UnityServices.InitializeAsync();
        if (!AuthenticationService.Instance.IsSignedIn)
        {
            await AuthenticationService.Instance.SignInAnonymouslyAsync();
        }
        HandleSignIn.Invoke();
        LobbyProvider.Instance.GetLobbies();
    }
}