using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;
using UnityEngine.UI;

public class LobbyObject : MonoBehaviour
{
    public TextMeshProUGUI Title;

    public Button SelectButton;

    private Lobby _lobbyInfo;

    private string _originalLobbyName;
    private LobbyEventCallbacks _callbacks;
    public async Task Host(string lobbyName)
    {
        Title.text = lobbyName;
        _originalLobbyName = lobbyName;
        SelectButton.interactable = false;
        _lobbyInfo = await CreateLobby(lobbyName, 2);

        _callbacks = new LobbyEventCallbacks();
        _callbacks.LobbyChanged += HandleHostLobbyChanged;

        await Lobbies.Instance.SubscribeToLobbyEventsAsync(_lobbyInfo.Id, _callbacks);
        StartCoroutine(HeartbeatLobbyCoroutine(15));

        RiddleProvider.Instance.ClearText();
        RiddleProvider.Instance.ReceiveState();
        RiddleProvider.Instance.ToggleRiddlePanel(true);
        RiddleProvider.Instance.SendButton.interactable = false;
        RiddleProvider.Instance.SendAnswer.AddListener(SendAnswerToClient);
        RiddleProvider.Instance.AnsweredPanel.SetActive(false);
        RiddleProvider.Instance.DeliveredPanel.SetActive(false);
    }

    public string GetLobbyId()
    {
        return _lobbyInfo.Id;
    }
    
    public void HandleJoin(Lobby lobbyInfo)
    {
        _lobbyInfo = lobbyInfo;
        Title.text = _lobbyInfo.Name;
    }
    
    private void HandleHostLobbyChanged(ILobbyChanges obj)
    {
        if (obj.PlayerData.Changed)
        {
            RiddleProvider.Instance.AnsweredPanel.SetActive(false);
            foreach (var keyValue in obj.PlayerData.Value)
            {
                foreach (var change in keyValue.Value.ChangedData.Value)
                {
                    string result = change.Value.Value.Value;
                    if (change.Key == "Riddle")
                    {
                        var image = RiddleProvider.Instance.AnwserField.GetComponent<Image>();
                        if(image.color == Color.red || image.color == Color.green)
                            image.color = Color.white;
                        RiddleProvider.Instance.RiddleHolder.text = change.Value.Value.Value;
                        RiddleProvider.Instance.AnwserField.text = "";
                        RiddleProvider.Instance.CorrectAnwser.text = "";
                        RiddleProvider.Instance.SendButton.interactable = true;
                    }

                    if (change.Key == "Result")
                    {
                        RiddleProvider.Instance.AnwserField.GetComponent<Image>().color =
                            result.Contains("true") ? Color.green : Color.red;
                        Debug.Log(change.Value.Value.Value);
                        if (result.Contains("true"))
                        {
                            GameManager.Score++;
                            UpdateHostScore();
                        }
                        
                        RiddleProvider.Instance.SendButton.interactable = false;
                    }

                    if (change.Key == "Answer")
                    {
                        RiddleProvider.Instance.CorrectAnwser.text = result;
                    }
                }
            }
        }
    }

    private async void UpdateHostScore()
    {
        try
        {
            var score = GameManager.Score > 0 ? $" ({GameManager.Score} solved)" : "";
            var options = new UpdateLobbyOptions
            {
                Name = _originalLobbyName + score
            };

            _lobbyInfo = await LobbyService.Instance.UpdateLobbyAsync(_lobbyInfo.Id, options);
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    private async void SendAnswerToClient(string answer)
    {
        try
        {
            var options = new UpdateLobbyOptions
            {
                Data = new Dictionary<string, DataObject>()
                {
                    {
                        "Answer", new DataObject(
                            visibility: DataObject.VisibilityOptions.Public,
                            value: answer)
                    }
                }
            };

            _lobbyInfo = await LobbyService.Instance.UpdateLobbyAsync(_lobbyInfo.Id, options);
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    private void HandleClientLobbyChanged(ILobbyChanges obj)
    {
        if(obj.LobbyDeleted)
            RiddleProvider.Instance.ToggleRiddlePanel(false);
        if (!obj.Data.Changed) return;
        foreach (var keyValue in obj.Data.Value)
        {
            var value = keyValue.Value.Value.Value;
            RiddleProvider.Instance.AnwserField.text = value;
            var compare = RiddleProvider.Correct(value.ToLower().Trim(' '), RiddleProvider.Instance.CurrentRiddleObj.answer.ToLower().Trim(' '));
            if (compare)
            {
                SendResultToHost("true");
            }
            else
            {
                RiddleProvider.Instance.Accept.onClick.AddListener(() => SendResultToHost("true"));
                RiddleProvider.Instance.Refuse.onClick.AddListener(() => SendResultToHost("false"));
                RiddleProvider.Instance.Accept.gameObject.SetActive(true);
                RiddleProvider.Instance.Refuse.gameObject.SetActive(true);
            }

            RiddleProvider.Instance.AnwserField.GetComponent<Image>().color =
                compare ? Color.green : Color.red;
        }
    }

    private async Task<Lobby> CreateLobby(string lobbyName, int maxPlayers)
    {
        var lobby = await LobbyService.Instance.CreateLobbyAsync(
            lobbyName,
            maxPlayers,
            CreatePublicOptions(lobbyName));
        return lobby;
    }

    private CreateLobbyOptions CreatePublicOptions(string lobbyName)
    {
        return new CreateLobbyOptions
        {
            Data = new Dictionary<string, DataObject>()
            {
                { lobbyName, new DataObject(visibility: DataObject.VisibilityOptions.Public) }
            },
            Player = new Player(id: AuthenticationService.Instance.PlayerId)
        };
    }

    public async void JoinLobby()
    {
        RiddleProvider.Instance.ToggleRiddlePanel(true);
        var image = RiddleProvider.Instance.AnwserField.GetComponent<Image>();
        if(image.color == Color.red || image.color == Color.green)
            image.color = Color.white;
        try
        {
            var options = new JoinLobbyByIdOptions()
            {
                Player = new Player(id: AuthenticationService.Instance.PlayerId)
            };
            await LobbyService.Instance.JoinLobbyByIdAsync(_lobbyInfo.Id, options);
            _callbacks = new LobbyEventCallbacks();
            _callbacks.LobbyChanged += HandleClientLobbyChanged;

            await Lobbies.Instance.SubscribeToLobbyEventsAsync(_lobbyInfo.Id, _callbacks);
            RiddleProvider.Instance.SendState();
            RiddleProvider.Instance.ReceiveRiddle.AddListener(SendRiddleToHost);
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    private async void SendRiddleToHost(string riddle)
    {
        try
        {
            UpdatePlayerOptions options = new UpdatePlayerOptions
            {
                Data = new Dictionary<string, PlayerDataObject>()
                {
                    {
                        "Riddle", new PlayerDataObject(
                            visibility: PlayerDataObject.VisibilityOptions.Public,
                            value: riddle)
                    }
                }
            };

            string playerId = AuthenticationService.Instance.PlayerId;

            _lobbyInfo = await LobbyService.Instance.UpdatePlayerAsync(_lobbyInfo.Id, playerId, options);
            RiddleProvider.Instance.ShuffleButton.interactable = false;
            RiddleProvider.Instance.SendButton.interactable = false;
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    private async void SendResultToHost(string result)
    {
        RiddleProvider.Instance.Accept.gameObject.SetActive(false);
        RiddleProvider.Instance.Refuse.gameObject.SetActive(false);
        try
        {
            UpdatePlayerOptions options = new UpdatePlayerOptions
            {
                Data = new Dictionary<string, PlayerDataObject>()
                {
                    {
                        "Result", new PlayerDataObject(
                            visibility: PlayerDataObject.VisibilityOptions.Public,
                            value: result)
                    },
                    {
                        "Answer", new PlayerDataObject(
                            visibility: PlayerDataObject.VisibilityOptions.Public,
                            value: RiddleProvider.Instance.CurrentRiddleObj.answer)
                    }
                }
            };

            string playerId = AuthenticationService.Instance.PlayerId;

            _lobbyInfo = await LobbyService.Instance.UpdatePlayerAsync(_lobbyInfo.Id, playerId, options);
            RiddleProvider.Instance.AnwserField.GetComponent<Image>().color = Color.white;
            RiddleProvider.Instance.ShuffleButton.interactable = true;
        }
        catch (LobbyServiceException e)
        {
            Debug.Log(e);
        }
    }

    IEnumerator HeartbeatLobbyCoroutine(float waitTimeSeconds)
    {
        var delay = new WaitForSecondsRealtime(waitTimeSeconds);

        while (isActiveAndEnabled)
        {
            LobbyService.Instance.SendHeartbeatPingAsync(_lobbyInfo.Id);
            yield return delay;
            Debug.Log("HearthBeat");
        }
    }

    public void OnDestroy()
    {
        if (_lobbyInfo.HostId == AuthenticationService.Instance.PlayerId)
            LobbyService.Instance.DeleteLobbyAsync(_lobbyInfo.Id);
    }
}